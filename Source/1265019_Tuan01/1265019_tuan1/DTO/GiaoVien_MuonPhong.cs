﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class GiaoVien_MuonPhong
    {
        private string _MaPhong;
        public string MaPhong 
        { 
            get{return _MaPhong;}
            set { _MaPhong = value; }
        }
        private string _MaGV;
        public string MaGV
        {
            get { return _MaGV; }
            set { _MaGV = value; }
        }
        private int _TietBD;
        public int TietBD
        {
            get { return _TietBD; }
            set { _TietBD = value; }
        }
        private int _TietKT;
        public int TietKT
        {
            get { return _TietKT; }
            set { _TietKT = value; }
        }
        private DateTime _Ngay;
        public DateTime Ngay
        {
            get { return _Ngay; }
            set { _Ngay = value; }
        }
    }
}
