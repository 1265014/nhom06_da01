﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class PhongHocDTO
    {
        private string _MaPhong;
        public string MaPhong
        {
            get { return _MaPhong; }
            set {_MaPhong = value;}
        }
        private string _TenPhong;
        public string TenPhong
        {
            get { return _TenPhong; }
            set { _TenPhong = value; }

        }
        private string _LoaiPhong;
        public string LoaiPhong
        {
            get { return _LoaiPhong; }
            set { _LoaiPhong = value; }
        }
    }
}
