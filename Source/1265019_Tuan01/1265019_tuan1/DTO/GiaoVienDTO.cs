﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class GiaoVienDTO
    {
        private string _MaGV;
        public string MaGV
        {
            get { return _MaGV; }
            set { _MaGV = value; }

        }
        private string _TenGV;
        public string TenGV
        {
            get { return _TenGV; }
            set { _TenGV = value; }

        }
        private string _GioiTinh;
        public string GioiTinh
        {
            get { return _GioiTinh; }
            set { _GioiTinh = value; }

        }
        private string _DiaChi;
        public string DiaChi
        {
            get { return _DiaChi; }
            set { _DiaChi = value; }

        }
        private DateTime _NgaySinh;
        public DateTime NgaySinh
        {
            get { return _NgaySinh; }
            set { _NgaySinh = value; }

        }
        private string _DienThoai;
        public string DienThoai
        {
            get { return _DienThoai; }
            set { _DienThoai = value; }

        }
    }
}
