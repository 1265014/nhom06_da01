﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DTO;
namespace DAO
{
    public  class GiaoVien_PhongHocDAO
    {
        public List<GiaoVien_MuonPhong> dsm()
        {
            string query= "select * from GIAOVIEN_PHONGHOC";
            SqlConnection con = new SqlConnection();
            DataProvider da = new DataProvider();
            con = da.Connection();
            SqlCommand cmd = new SqlCommand(query,con);
            SqlDataReader reader = cmd.ExecuteReader();
            List<GiaoVien_MuonPhong> ds = new List<GiaoVien_MuonPhong>();
            while (reader.Read())
            {
                GiaoVien_MuonPhong gvm = new GiaoVien_MuonPhong();
                gvm.MaPhong = reader.GetString(0);
                gvm.MaGV = reader.GetString(1);
                gvm.TietBD = reader.GetInt32(2);
                gvm.TietKT = reader.GetInt32(3);
                gvm.Ngay = reader.GetDateTime(4);
                
                ds.Add(gvm);
            }
            con.Close();
            return ds;
        }
    }
}
