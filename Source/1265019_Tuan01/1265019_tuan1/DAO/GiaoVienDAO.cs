﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;

namespace DAO
{
    public  class GiaoVienDAO
    {
        public List<GiaoVienDTO> dsGV()
        {
            string query = "select * from GIAOVIEN";
            DataProvider da = new DataProvider();
            SqlConnection con = da.Connection();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            List<GiaoVienDTO> ds = new List<GiaoVienDTO>();
            while (reader.Read())
            {
                GiaoVienDTO gv = new GiaoVienDTO();
                gv.MaGV = reader.GetString(0);
                gv.TenGV = reader.GetString(1);
                gv.GioiTinh = reader.GetString(2);
                gv.DiaChi = reader.GetString(3);
                gv.NgaySinh = reader.GetDateTime(4);
                gv.DienThoai = reader.GetString(5);

                ds.Add(gv);

            }
            con.Close();
            return ds;


        }
    }
}
