﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
namespace DAO
{
    public class PhongHocDAO
    {
        public List<PhongHocDTO> dsPH()
        {
            string query = "Select * from PHONGHOC";
            DataProvider da = new DataProvider();
            SqlConnection con = da.Connection();
            SqlCommand cmd = new SqlCommand(query,con);
            SqlDataReader reader = cmd.ExecuteReader();
            List<PhongHocDTO> ds = new List<PhongHocDTO>();
            while (reader.Read())
            {
                PhongHocDTO ph = new PhongHocDTO();
                ph.MaPhong = reader.GetString(0);
                ph.TenPhong = reader.GetString(1);
                ph.LoaiPhong = reader.GetString(2);
                ds.Add(ph);
            }
            con.Close();
            return ds;

        }
    }
}
