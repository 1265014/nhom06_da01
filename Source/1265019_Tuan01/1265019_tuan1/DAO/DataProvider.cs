﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace DAO
{
    public class DataProvider
    {
        public SqlConnection Connection ()
        {
            string contr = ConfigurationManager.ConnectionStrings["Connect"].ToString();
            SqlConnection con = new SqlConnection(contr);
            con.Open();
            return con;
        }
        
    }
}
