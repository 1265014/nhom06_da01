﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DTO;
using BUS;
namespace GiaoDien
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //List<GiaoVienDTO> ds = new List<GiaoVienDTO>();
                GiaoVienBUS gvBUS = new GiaoVienBUS();
                cbTenGV.DataSource = gvBUS.DSGV();
                cbTenGV.DisplayMember = "MaGV";
                cbTenGV.ValueMember = "MaGV";
                PhongHocBUS phBUS = new PhongHocBUS();
                List<PhongHocDTO> ds = new List<PhongHocDTO>();
                ds = phBUS.DSPH();
                foreach (PhongHocDTO ph in ds )
                {
                    cbPhong.Items.Add(ph.MaPhong);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

  
        }

        private void btnMuon_Click(object sender, EventArgs e)
        {
            GiaoVien_PhongHocBUS gvphBUS = new GiaoVien_PhongHocBUS();
            List<GiaoVien_MuonPhong> ds = new List<GiaoVien_MuonPhong>();
            ds = gvphBUS.DSM();
            int temp =0;
            foreach (GiaoVien_MuonPhong gvmp in ds)
            {
                if (cbPhong.Text == gvmp.MaPhong.ToString() && txtTietBD.Text == gvmp.TietBD.ToString() && txtTietKT.Text == gvmp.TietKT.ToString() && dtpNgay.Value.ToShortDateString() == gvmp.Ngay.ToShortDateString())
                {
                    temp = 1;
                    //MessageBox.Show(dtpNgay.Value.ToShortDateString());
                }
                
            }
            if (temp == 1)
            {
                MessageBox.Show("Đã bị đặt trước !!");

            }
            else if (temp == 0)
            {
                MessageBox.Show("Mượn thành công !!");
            }
        }
    }
}
