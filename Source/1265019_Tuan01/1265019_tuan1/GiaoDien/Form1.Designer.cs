﻿namespace GiaoDien
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTIm = new System.Windows.Forms.Button();
            this.cbTenGV = new System.Windows.Forms.ComboBox();
            this.cbPhong = new System.Windows.Forms.ComboBox();
            this.dtpNgay = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnMuon = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTietBD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTietKT = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnTIm
            // 
            this.btnTIm.Location = new System.Drawing.Point(15, 112);
            this.btnTIm.Name = "btnTIm";
            this.btnTIm.Size = new System.Drawing.Size(75, 23);
            this.btnTIm.TabIndex = 0;
            this.btnTIm.Text = "Khỡi tạo";
            this.btnTIm.UseVisualStyleBackColor = true;
            // 
            // cbTenGV
            // 
            this.cbTenGV.FormattingEnabled = true;
            this.cbTenGV.Location = new System.Drawing.Point(118, 35);
            this.cbTenGV.Name = "cbTenGV";
            this.cbTenGV.Size = new System.Drawing.Size(121, 21);
            this.cbTenGV.TabIndex = 1;
            // 
            // cbPhong
            // 
            this.cbPhong.FormattingEnabled = true;
            this.cbPhong.Location = new System.Drawing.Point(358, 35);
            this.cbPhong.Name = "cbPhong";
            this.cbPhong.Size = new System.Drawing.Size(121, 21);
            this.cbPhong.TabIndex = 2;
            // 
            // dtpNgay
            // 
            this.dtpNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgay.Location = new System.Drawing.Point(118, 71);
            this.dtpNgay.Name = "dtpNgay";
            this.dtpNgay.Size = new System.Drawing.Size(121, 20);
            this.dtpNgay.TabIndex = 3;
            this.dtpNgay.Value = new System.DateTime(2014, 11, 24, 12, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Chọn giáo viên";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ngày";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(265, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Chọn phòng";
            // 
            // btnMuon
            // 
            this.btnMuon.Location = new System.Drawing.Point(118, 112);
            this.btnMuon.Name = "btnMuon";
            this.btnMuon.Size = new System.Drawing.Size(75, 23);
            this.btnMuon.TabIndex = 7;
            this.btnMuon.Text = "Mượn";
            this.btnMuon.UseVisualStyleBackColor = true;
            this.btnMuon.Click += new System.EventHandler(this.btnMuon_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(265, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nhập tiết bắt đâu";
            // 
            // txtTietBD
            // 
            this.txtTietBD.Location = new System.Drawing.Point(361, 74);
            this.txtTietBD.Name = "txtTietBD";
            this.txtTietBD.Size = new System.Drawing.Size(49, 20);
            this.txtTietBD.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(416, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Tiết kết thúc";
            // 
            // txtTietKT
            // 
            this.txtTietKT.Location = new System.Drawing.Point(485, 77);
            this.txtTietKT.Name = "txtTietKT";
            this.txtTietKT.Size = new System.Drawing.Size(40, 20);
            this.txtTietKT.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 147);
            this.Controls.Add(this.txtTietKT);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTietBD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnMuon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpNgay);
            this.Controls.Add(this.cbPhong);
            this.Controls.Add(this.cbTenGV);
            this.Controls.Add(this.btnTIm);
            this.Name = "Form1";
            this.Text = "Mượn Phòng";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTIm;
        private System.Windows.Forms.ComboBox cbTenGV;
        private System.Windows.Forms.ComboBox cbPhong;
        private System.Windows.Forms.DateTimePicker dtpNgay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnMuon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTietBD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTietKT;
    }
}

