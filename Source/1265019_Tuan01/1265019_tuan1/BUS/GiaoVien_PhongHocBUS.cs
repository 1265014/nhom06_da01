﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;

namespace BUS
{
    public class GiaoVien_PhongHocBUS
    {
        public List<GiaoVien_MuonPhong> DSM()
        {
            List<GiaoVien_MuonPhong> dsGVM = new List<GiaoVien_MuonPhong>();
            GiaoVien_PhongHocDAO gvDAO = new GiaoVien_PhongHocDAO();
            dsGVM = gvDAO.dsm();
            return dsGVM;
        }
    }
}
