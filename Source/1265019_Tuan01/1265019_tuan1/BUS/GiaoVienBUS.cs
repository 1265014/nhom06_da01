﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAO;
using DTO;

namespace BUS
{
    public class GiaoVienBUS
    {
        public List<GiaoVienDTO> DSGV()
        {
            List<GiaoVienDTO> dsgv = new List<GiaoVienDTO>();
        
            GiaoVienDAO gvDAO = new GiaoVienDAO();
            dsgv = gvDAO.dsGV();
            return dsgv;
        }
    }
}
