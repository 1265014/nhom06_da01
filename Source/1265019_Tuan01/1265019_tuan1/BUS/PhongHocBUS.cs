﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
namespace BUS
{
    public class PhongHocBUS
    {
        public List<PhongHocDTO> DSPH()
        {

            List<PhongHocDTO> dsph = new List<PhongHocDTO>();
            PhongHocDAO phDAO = new PhongHocDAO();
            dsph = phDAO.dsPH();
            return dsph;

        }
    }
}
