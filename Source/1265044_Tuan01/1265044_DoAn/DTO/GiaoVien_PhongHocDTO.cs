﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class GiaoVien_PhongHocDTO
    {
        private string MaPhong;
        public string MaPhongs
        {
            get
            {
                return MaPhong;
            }
            set
            {
                MaPhong = value;
            }
        }
        private string MaGV;
        public string Magvs
        {
            get
            {
                return MaGV;
            }
            set
            {
                MaGV = value;
            }
        }
        private int TietBD;
        public int TietBDs
        {
            get
            {
                return TietBD;
            }
            set
            {
                TietBD = value;
            }
        }
        private int TietKT;
        public int TietKTs
        {
            get
            {
                return TietKT;
            }
            set
            {
                TietKT = value;
            }
        }
        private DateTime NgayMuon;
        public DateTime NgayMuons
        {
            get
            {
                return NgayMuon;
            }
            set
            {
                NgayMuon = value;
            }
        }
    
    }
}
