﻿namespace frmXemLichDay
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_tt = new System.Windows.Forms.Label();
            this.DGV_DS = new System.Windows.Forms.DataGridView();
            this.MaPhong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tietbd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TietKT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayMuon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_DS)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_tt
            // 
            this.lbl_tt.AutoSize = true;
            this.lbl_tt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tt.Location = new System.Drawing.Point(222, 23);
            this.lbl_tt.Name = "lbl_tt";
            this.lbl_tt.Size = new System.Drawing.Size(151, 24);
            this.lbl_tt.TabIndex = 0;
            this.lbl_tt.Text = "Lịch Giảng Dạy";
            // 
            // DGV_DS
            // 
            this.DGV_DS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_DS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaPhong,
            this.MaGV,
            this.Tietbd,
            this.TietKT,
            this.NgayMuon});
            this.DGV_DS.Location = new System.Drawing.Point(12, 67);
            this.DGV_DS.Name = "DGV_DS";
            this.DGV_DS.Size = new System.Drawing.Size(592, 172);
            this.DGV_DS.TabIndex = 1;
            // 
            // MaPhong
            // 
            this.MaPhong.HeaderText = "Mã Phòng";
            this.MaPhong.Name = "MaPhong";
            this.MaPhong.ReadOnly = true;
            // 
            // MaGV
            // 
            this.MaGV.HeaderText = "Mã Giao Viên";
            this.MaGV.Name = "MaGV";
            this.MaGV.ReadOnly = true;
            // 
            // Tietbd
            // 
            this.Tietbd.HeaderText = "Tiết Bắt Đầu";
            this.Tietbd.Name = "Tietbd";
            this.Tietbd.ReadOnly = true;
            // 
            // TietKT
            // 
            this.TietKT.HeaderText = "Tiết Kết Thúc";
            this.TietKT.Name = "TietKT";
            this.TietKT.ReadOnly = true;
            // 
            // NgayMuon
            // 
            this.NgayMuon.HeaderText = "Ngày Mượn";
            this.NgayMuon.Name = "NgayMuon";
            this.NgayMuon.ReadOnly = true;
            this.NgayMuon.Width = 150;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 255);
            this.Controls.Add(this.DGV_DS);
            this.Controls.Add(this.lbl_tt);
            this.Name = "Form1";
            this.Text = "Xem Lịch Giảng Dạy";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_DS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_tt;
        private System.Windows.Forms.DataGridView DGV_DS;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaPhong;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tietbd;
        private System.Windows.Forms.DataGridViewTextBoxColumn TietKT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayMuon;
    }
}

