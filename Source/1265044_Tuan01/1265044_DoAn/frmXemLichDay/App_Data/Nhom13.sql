CREATE DATABASE QLTB_NHOM6
GO
USE QLTB_NHOM6
GO

CREATE TABLE PHONGHOC
(
MaPhong CHAR(5),
TenPhong CHAR(5),
LoaiPhong NVARCHAR(20),
CONSTRAINT pk_PHONGHOC PRIMARY KEY (MaPhong),
)
GO

CREATE TABLE SUDUNG_PHONGHOC
(
	MaSDP CHAR(5),
	MaPhong CHAR(5),
	MaPhongDuTru char(5), 
	TietBD INT, 
	TietKT INT,
	Ngay DATETIME,
	CONSTRAINT pk_SUDUNG_PHONGHOC PRIMARY KEY (MaSDP),
)
CREATE TABLE THIETBI
(
	MaTB CHAR(5),
	TenTB NVARCHAR(20),
	MaLoaiTB CHAR(5),
	TinhTrang NVARCHAR(20),
	CONSTRAINT pk_THIETBI PRIMARY KEY (MaTB),
)
GO

CREATE TABLE LOAITHIETBI
(
	MaLoaiTB CHAR(5),
	TenLoaiTB NVARCHAR(20),
	SoLuong INT,
	CONSTRAINT pk_LOAITHIETBI PRIMARY KEY (MaLoaiTB),
)
GO

CREATE TABLE GIAOVIEN_PHONGHOC
(
	MaPhong CHAR(5),
	MaGV NCHAR(10),
	TietBD INT,
	TietKT INT,
	NgayMuon DATETIME,
	
	CONSTRAINT pk_GIAOVIEN_PHONGHOC PRIMARY KEY (MaPhong, MaGV),
)
GO

CREATE TABLE SINHVIEN_THIETBI
(
	MaTB CHAR(5),
	MaSV NCHAR(10),
	MaPhong CHAR(5),
	TietBD INT,
	TietKT INT,
	NgayMuon DATETIME,
	
	CONSTRAINT pk_SINHVIEN_THIETBI PRIMARY KEY (MaTB, MaSV),
)
GO

CREATE TABLE GIAOVIEN 
(
	MaGV NCHAR(10) not null, 
	TenGV NVARCHAR(50),  
	GioiTinh NCHAR(10), 
	DiaChi NVARCHAR(50),
	NgaySinh datetime, 
	DienThoai varCHAR(20),
	
	CONSTRAINT pk_GIAOVIEN PRIMARY KEY (MaGV) 
)
GO

CREATE TABLE SINHVIEN 
(
	MaSV NCHAR(10) not null, 
	TenSV NVARCHAR(50), 
	Lop NCHAR(10), 
	GioiTinh NCHAR(10), 
	DiaChi NVARCHAR(50), 
	NgaySinh datetime, 
	DienThoai varCHAR(20), 
	
	CONSTRAINT pk_SINHVIEN PRIMARY KEY (MaSV) 
)
GO

CREATE TABLE TAIKHOAN 
(
	MaTK NCHAR(10), 
	TenTK varCHAR(50), 
	MatKhau varCHAR(50), 
	MaLoai NCHAR(10), 
	
	CONSTRAINT pk_TAIKHOAN PRIMARY KEY (MaTK) 
)
GO

CREATE TABLE LOAITAIKHOAN
(
	MaLoaiTK NCHAR(10), 
	TenLoaiTK NVARCHAR(50), 
	
	CONSTRAINT pk_LOAITAIKHOAN PRIMARY KEY (MaLoaiTK) 
)
GO

Alter table GIAOVIEN_PHONGHOC
Add constraint fk_GVPH_GV foreign key (MaGV) references GIAOVIEN(MaGV)
Alter table GIAOVIEN_PHONGHOC
Add constraint fk_GVPG_PH foreign key (MaPhong) references PHONGHOC(MaPhong)

Alter table SINHVIEN_THIETBI
Add constraint fk_SVTB_SV foreign key (MaSV) references SINHVIEN(MaSV)
Alter table SINHVIEN_THIETBI
Add constraint fk_SVTB_TB foreign key (MaTB) references THIETBI(MaTB)
Alter table SINHVIEN_THIETBI
Add constraint fk_SVTB_PH foreign key (MaPhong) references PHONGHOC(MaPhong)


INSERT INTO GIAOVIEN(MaGV, TenGV, GioiTinh, DiaChi, NgaySinh, DienThoai) Values ('GV01', N'Trần Văn A', N'Nam', N'1 Hồng Bàng, TP HCM', '01-01-1985', '0123456789')
INSERT INTO GIAOVIEN(MaGV, TenGV, GioiTinh, DiaChi, NgaySinh, DienThoai) Values ('GV02', N'Trần Thị B', N'Nữ', N'2 Hồng Bàng, TP HCM', '02-01-1985', '0123456789')
INSERT INTO GIAOVIEN(MaGV, TenGV, GioiTinh, DiaChi, NgaySinh, DienThoai) Values ('GV03', N'Trần Văn C', N'Nam', N'3 Hồng Bàng, TP HCM', '03-01-1985', '0123456789')
INSERT INTO GIAOVIEN(MaGV, TenGV, GioiTinh, DiaChi, NgaySinh, DienThoai) Values ('GV04', N'Trần Thị D', N'Nữ', N'4 Hồng Bàng, TP HCM', '04-01-1985', '0123456789')
INSERT INTO GIAOVIEN(MaGV, TenGV, GioiTinh, DiaChi, NgaySinh, DienThoai) Values ('GV05', N'Trần Văn E', N'Nam', N'5 Hồng Bàng, TP HCM', '05-01-1985', '0123456789')

INSERT INTO SINHVIEN(MaSV, TenSV, Lop, DiaChi, GioiTinh, NgaySinh, DienThoai) Values ('SV01', N'Sinh Viên 01', '12CK1', N'1 Huyền Trân Công Chúa, TP HCM', N'Nữ', '01-01-1994', '0123456789')
INSERT INTO SINHVIEN(MaSV, TenSV, Lop, DiaChi, GioiTinh, NgaySinh, DienThoai) Values ('SV02', N'Sinh Viên 02', '12CK2', N'2 Huyền Trân Công Chúa, TP HCM', N'Nam', '01-02-1994', '0123456789')
INSERT INTO SINHVIEN(MaSV, TenSV, Lop, DiaChi, GioiTinh, NgaySinh, DienThoai) Values ('SV03', N'Sinh Viên 03', '12CK3', N'3 Huyền Trân Công Chúa, TP HCM', N'Nữ', '01-03-1994', '0123456789')
INSERT INTO SINHVIEN(MaSV, TenSV, Lop, DiaChi, GioiTinh, NgaySinh, DienThoai) Values ('SV04', N'Sinh Viên 04', '12CK4', N'4 Huyền Trân Công Chúa, TP HCM', N'Nam', '01-04-1994', '0123456789')
INSERT INTO SINHVIEN(MaSV, TenSV, Lop, DiaChi, GioiTinh, NgaySinh, DienThoai) Values ('SV05', N'Sinh Viên 05', '12CK5', N'5 Huyền Trân Công Chúa, TP HCM', N'Nữ', '01-05-1994', '0123456789')


INSERT INTO LOAITHIETBI(MaLoaiTB, TenLoaiTB, SoLuong) Values ('LTB01', N'Micrô', 2)
INSERT INTO LOAITHIETBI(MaLoaiTB, TenLoaiTB, SoLuong) Values ('LTB02', N'Máy chiếu', 1)
INSERT INTO LOAITHIETBI(MaLoaiTB, TenLoaiTB, SoLuong) Values ('LTB03', N'Đá địa chất', 1)

INSERT INTO THIETBI(MaTB, TenTB, MaLoaiTB, TinhTrang) VALUES ('TB01', N'micro 1', 'LTB01', N'Còn dùng được')
INSERT INTO THIETBI(MaTB, TenTB, MaLoaiTB, TinhTrang) VALUES ('TB02', N'micro 2', 'LTB01', N'Còn dùng được')
INSERT INTO THIETBI(MaTB, TenTB, MaLoaiTB, TinhTrang) VALUES ('TB03', N'Máy chiếu 3', 'LTB02', N'Còn dùng được')
INSERT INTO THIETBI(MaTB, TenTB, MaLoaiTB, TinhTrang) VALUES ('TB04', N'Đá Xanh 4', 'LTB03', N'Còn dùng được')

INSERT INTO PHONGHOC(MaPhong, TenPhong, LoaiPhong) Values ('PH01','C21', N'Phòng thường')
INSERT INTO PHONGHOC(MaPhong, TenPhong, LoaiPhong) Values ('PH02','C22', N'Phòng thường')
INSERT INTO PHONGHOC(MaPhong, TenPhong, LoaiPhong) Values ('PH03','C23', N'Phòng thường')
INSERT INTO PHONGHOC(MaPhong, TenPhong, LoaiPhong) Values ('PH04','C24', N'Phòng máy')

INSERT INTO SUDUNG_PHONGHOC(MaSDP, MaPhong, MaPhongDuTru, TietBD, TietKT, Ngay) Values ('SDP01','PH01', 'PH02', '1', '3', '01-12-2014')
INSERT INTO SUDUNG_PHONGHOC(MaSDP, MaPhong, MaPhongDuTru, TietBD, TietKT, Ngay) Values ('SDP02','PH02', 'PH03', '1', '6', '01-12-2014')
INSERT INTO SUDUNG_PHONGHOC(MaSDP, MaPhong, MaPhongDuTru, TietBD, TietKT, Ngay) Values ('SDP03','PH03', 'PH04', '6', '9', '02-12-2014')
INSERT INTO SUDUNG_PHONGHOC(MaSDP, MaPhong, MaPhongDuTru, TietBD, TietKT, Ngay) Values ('SDP04','PH04', 'PH01', '1', '3', '03-12-2014')

INSERT INTO GIAOVIEN_PHONGHOC(MaPhong, MaGV, TietBD, TietKT, NgayMuon) VALUES ('PH01', 'GV01', '1','3', '01-12-2014')
INSERT INTO GIAOVIEN_PHONGHOC(MaPhong, MaGV, TietBD, TietKT, NgayMuon) VALUES ('PH02', 'GV02', '1','6', '01-12-2014')
INSERT INTO GIAOVIEN_PHONGHOC(MaPhong, MaGV, TietBD, TietKT, NgayMuon) VALUES ('PH03', 'GV03', '6','9', '02-12-2014')
INSERT INTO GIAOVIEN_PHONGHOC(MaPhong, MaGV, TietBD, TietKT, NgayMuon) VALUES ('PH04', 'GV04', '1','3', '03-12-2014')

INSERT INTO SINHVIEN_THIETBI(MaTB, MaSV, MaPhong, TietBD, TietKT, NgayMuon) VALUES ('TB01', 'SV01', 'PH01', '1', '3', '01-12-2014')
INSERT INTO SINHVIEN_THIETBI(MaTB, MaSV, MaPhong, TietBD, TietKT, NgayMuon) VALUES ('TB02', 'SV02', 'PH02', '1', '6', '01-12-2014')
INSERT INTO SINHVIEN_THIETBI(MaTB, MaSV, MaPhong, TietBD, TietKT, NgayMuon) VALUES ('TB03', 'SV03', 'PH03', '6', '9', '02-12-2014')
INSERT INTO SINHVIEN_THIETBI(MaTB, MaSV, MaPhong, TietBD, TietKT, NgayMuon) VALUES ('TB04', 'SV04', 'PH04', '1', '3', '03-12-2014')

INSERT INTO LOAITAIKHOAN(MaLoaiTK, TenLoaiTK) VALUES ('LTK01', N'Trưởng phòng quản trị thiết bị')
INSERT INTO LOAITAIKHOAN(MaLoaiTK, TenLoaiTK) VALUES ('LTK02', N'Nhân viên trực')

INSERT INTO TAIKHOAN(MaTK, TenTK, MatKhau, MaLoai) VALUES ('TK01', 'NV01', '123456', 'LTK01')
INSERT INTO TAIKHOAN(MaTK, TenTK, MatKhau, MaLoai) VALUES ('TK02', 'NV02', '123456', 'LTK02')
INSERT INTO TAIKHOAN(MaTK, TenTK, MatKhau, MaLoai) VALUES ('TK03', 'NV03', '123456', 'LTK02')
