﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DTO;
using BUS;

namespace frmXemLichDay
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<GiaoVien_PhongHocDTO> gv_ph = new List<GiaoVien_PhongHocDTO>();
            GiaoVien_PhongHocBUS gv_bus = new GiaoVien_PhongHocBUS();
            gv_ph = gv_bus.dsphong();

            foreach (GiaoVien_PhongHocDTO gvph in gv_ph)
            {
                DGV_DS.Rows.Add(gvph.MaPhongs, gvph.Magvs, gvph.TietBDs, gvph.TietKTs, gvph.NgayMuons);
            }

        }
    }
}
