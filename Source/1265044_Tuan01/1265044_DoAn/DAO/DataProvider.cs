﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace DAO
{
    class DataProvider
    {
        public SqlConnection conn()
        {
            string _connectionstring = ConfigurationManager.ConnectionStrings["Connect"].ToString();
            SqlConnection connect = new SqlConnection(_connectionstring);
            connect.Open();
            return connect;
        }
    }
}
