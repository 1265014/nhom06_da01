﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
namespace DAO
{
    public class GiaoVien_PhongHocDAO
    {
        public List<GiaoVien_PhongHocDTO> dsphong()
        {
            string query = "select * from GIAOVIEN_PHONGHOC";
            DataProvider dt = new DataProvider();
            SqlConnection con = dt.conn();
            SqlCommand cm = new SqlCommand(query, con);
            SqlDataReader reader = cm.ExecuteReader();
            List<GiaoVien_PhongHocDTO> ds = new List<GiaoVien_PhongHocDTO>();
            while(reader.Read())
            {
                GiaoVien_PhongHocDTO gv_ph = new GiaoVien_PhongHocDTO();
                gv_ph.MaPhongs = reader.GetString(0);
                gv_ph.Magvs = reader.GetString(1);
                gv_ph.TietBDs = reader.GetInt32(2);
                gv_ph.TietKTs = reader.GetInt32(3);
                gv_ph.NgayMuons = reader.GetDateTime(4);
                ds.Add(gv_ph);
            }
            con.Close();
            return ds;
        }
    }
}
