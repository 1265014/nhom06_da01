﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;

namespace BUS
{
    public class GiaoVien_PhongHocBUS
    {
        public List<GiaoVien_PhongHocDTO> dsphong()
        {
            GiaoVien_PhongHocDAO gv = new GiaoVien_PhongHocDAO();
            return gv.dsphong();
        }
    }
}
